function page_loaded() {
    const form = document.querySelector('#form')
    const input = form.querySelector('#movieInput')
    const checkbox = form.querySelector('#favourite')
    const rightSide = document.querySelector('.right-side')
    const movieDB = {
        movies: []
    }

    form.addEventListener('submit', addMovie)

    function addMovie(e) {
        e.preventDefault()
        let newMovie = input.value
        if (movieDB.movies.includes(newMovie)) return
        input.value = ''
        placeNewItem(newMovie)
    }

    function placeNewItem(newMovie) {
        let trashCan = document.createElement('img')
        trashCan.src = "img/trash.png"
        trashCan.classList.add('trash')
        trashCan.addEventListener('click', deleteItem)
        let newItem = document.createElement('div')
        newItem.classList.add('movieItem')
        newItem.setAttribute('data-full', newMovie)
        if (checkbox.checked) {
            newItem.setAttribute('data-favourite', true)
            checkbox.checked = false
            console.log('Добавляем любимый фильм')
        }
        if (newMovie.length > 21) {
            newMovie = newMovie.slice(0, 21) + '...'
        }
        newItem.textContent = newMovie
        newItem.append(trashCan)
        placeAlphabetic(newItem)
    }

    function deleteItem(e) {
        let parent = e.target.parentElement
        let movieTitle = parent.getAttribute('data-full')
        for (let i = 0; i < movieDB.movies.length; i++) {
            if (movieTitle === movieDB.movies[i]) {
                movieDB.movies.splice(i, 1)
                break
            }
        }
        parent.remove()
    }

    function placeAlphabetic(item) {
        let movieTitle = item.getAttribute('data-full').toLowerCase()
        let movies = rightSide.children
        let i = 0
        while (i < movies.length && movies[i].getAttribute('data-full').toLowerCase() < movieTitle) {
            i++
        }
        if (i === 0) {
            rightSide.prepend(item)
        }
        else {
            movies[i - 1].after(item)
        }
        movieDB.movies.splice(i, 0, item.getAttribute('data-full'))
    }
}

document.addEventListener('DOMContentLoaded', page_loaded)